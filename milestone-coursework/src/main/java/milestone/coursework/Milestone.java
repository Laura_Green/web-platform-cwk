package milestone.coursework;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Milestone {

    private String name, description;
    private Date terminationDate, completionDate;   //deadline and actual date
    private int priority;

    protected Milestone(String nam, String descript, Date termination, int prior)
    {

        setName(nam);
        setDescription(descript);
        setTerminationDate(termination);
        setCompletionDate(null);
        setPriority(prior);

    }

    protected void setName(String nam){name = nam;}
    protected void setDescription(String descript){description = descript;}
    protected void setTerminationDate(Date termination){terminationDate=termination;}
    protected void setCompletionDate(Date completion){completionDate=completion;}
    protected void setPriority(Integer prior){priority=prior;}



    protected String getName(){return name;}
    protected String getDescription(){return description;}
    protected Date getTerminationDate(){return terminationDate;}
    protected Date getCompletionDate(){return completionDate;}
    protected Integer getPriority(){return priority;}


    public void terminate()
    {
        completionDate = Calendar.getInstance().getTime();
    }


    public String toString() {

        String pattern = "MM/dd/yyyy HH:mm:ss";
        DateFormat df = new SimpleDateFormat(pattern);

        String completion = null;

        if(completionDate!=null) completion = df.format(completionDate) + "\n";

        return ("Name: " + name + "\n   Description: " + description + "\n  Terminate by " + df.format(terminationDate) + "\n " + completion);
    }




}
