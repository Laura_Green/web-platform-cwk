package milestone.coursework;

import java.util.Arrays;
import java.util.List;

public class Topic {

    private String topic;
    private List<String> messages;

    protected Topic(String t, List<String> n) {

        topic = "Event-based Architecture";
        messages = Arrays.asList("Cool", "Awesome", "Stuning");

    }
    public String getTopic() {
        return this.topic;
    }

    public List<String> getMessages() {
        return this.messages;
    }

    protected void setMessages(List<String> t) {
        this.messages = t;
    }

    protected void setTopic(String n) {
        this.topic = n;
    }

    public String toString() {
        return ("name: " + topic + ", messages: " + messages.toString());
    }
}
