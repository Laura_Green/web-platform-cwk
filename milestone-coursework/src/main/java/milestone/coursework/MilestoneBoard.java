package milestone.coursework;

import java.util.ArrayList;
import java.util.List;

public class MilestoneBoard {

    private String name;
    private List<Milestone> milestones;

    protected MilestoneBoard() {
        this.milestones = new ArrayList<>();
    }

    protected MilestoneBoard(String nam){
        this.milestones = new ArrayList<>();
        setName(nam);
    }

    protected MilestoneBoard(String nam, List<Milestone> t){
        setMilestones(t);
        setName(nam);
    }


    protected void setMilestones(List<Milestone> t) {
        this.milestones = t;
    }
    protected void setName(String n) {
        this.name = n;
    }


    public String getName() {
        return this.name;
    }
    public List<Milestone> getMilestones() {
        return this.milestones;
    }



    public void addMilestone(Milestone newMilestone){
        milestones.add(newMilestone);
    }

    public void deleteMilestone(Integer position){
        milestones.remove(position);
    }

    public void updateMilestone(Integer position, Milestone mil){
        milestones.set(position, mil);
    }





    public String toString() {
        return ("name: " + name + ", milestones: " + milestones.toString());
    }
}
