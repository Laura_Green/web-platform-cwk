package milestone.coursework;

import java.util.ArrayList;
import java.util.List;

public class MessageBoard {

    private String name;
    private List<String> topics;

    protected MessageBoard() {
        this.topics = new ArrayList<>();
    }

    protected MessageBoard(String Name){
        name = Name;
        this.topics = new ArrayList<>();
    }

    public String getName() {
        return this.name;
    }

    public List<String> getTopics() {
        return this.topics;
    }

    protected void setTopics(List<String> t) {
        this.topics = t;
    }

    protected void setName(String n) {
        this.name = n;
    }

    public void addTopic(String topic){
        topics.add(topic);
    }



    public String toString() {
        return ("name: " + name + ", topics: " + topics.toString());
    }
}
