package milestone.coursework;

import java.util.ArrayList;
import java.util.List;

public class MilestoneProjects {


    private String domain;
    private List<MilestoneBoard> projectList;

    protected MilestoneProjects() {
        this.projectList = new ArrayList<>();
    }

    protected MilestoneProjects(String dom){
        this.projectList = new ArrayList<>();
        setDomain(dom);
    }

    protected MilestoneProjects(String dom, List<MilestoneBoard> t){
        setProjectList(t);
        setDomain(dom);
    }


    protected void setProjectList(List<MilestoneBoard> t) {
        this.projectList = t;
    }
    protected void setDomain(String dom) { this.domain = dom; }


    public String getDomain() {
        return this.domain;
    }
    public List<MilestoneBoard> getMilestoneBoards() {
        return this.projectList;
    }



    public void addMilestoneBoard(MilestoneBoard newMilestoneBoard){
        projectList.add(newMilestoneBoard);
    }

    public void deleteMilestoneBoard(Integer position){
        projectList.remove(position);
    }



    public String toString() {
        return ("Domain: " + domain + "\n Projects: " + projectList.toString());
    }
}
